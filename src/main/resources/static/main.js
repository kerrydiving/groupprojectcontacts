let data = []
const state = {
    selectedContact: null
}

fetch('/contacts')
    .then(response => response.json())
    .then(_data => {
        data = _data
        sortContacts()
        // render()
    })
    .catch(err => console.error(err))

function render() {
    let content = data.map((contactData, i) => {
        // content.sort()
        return /*html*/ `
        <article>
            <button onclick="displayDetails(${i})">${contactData.contactName}</button>
        </article>
        `
    }).join("")

    // content += searchContactButton()
    content += createContactButton()

    const appElements = document.getElementById('app')
    appElements.innerHTML = content

    if (state.selectedContact) {
        const modalContent = /*html*/ `
            <section id="modal-bg">
                <article>
                    <img id="image-div" src="/photo/img_avatar.png" alt=”placeholder” height="50%" width="50%">
                    <div><i style="font-family: 'Calligraffitti';">Contact Name:</i></div>
                    <div>
                    ${state.selectedContact.contactName}
                    </div>
                    <div><i style="font-family: 'Calligraffitti';">Mobile Number:</i></div>
                    <div>
                    ${state.selectedContact.mobileNumber}
                    </div>
                    <div><i style="font-family: 'Calligraffitti';">Email Address:</i></div>
                    <div>
                    ${state.selectedContact.email}
                    </div>
                    <div><i style="font-family: 'Calligraffitti';">Address:</i></div>
                    <div>
                    ${state.selectedContact.address}
                    </div>
                    <section id="div-modal-buttons">
                    <button id="modal-button" onclick="closeModal()">Close</button>
                    <button id="modal-button" onclick="deleteContact()">Delete</button>
                    <button id="modal-button" onclick="loadUpdateContact()">Update</button>
                    </section>
                </article>
                

            </section>
        `
        console.log(modalContent)
        const modalEl = document.getElementById('modal')
        modalEl.innerHTML = modalContent
    } else {
        const modalEl = document.getElementById('modal')
        modalEl.innerHTML = ""
    }
}

function displayDetails(index) {
    console.log(data[index])
    state.selectedContact = data[index]
    sortContacts()
}
function closeModal() {
    state.selectedContact = null
    render()
}
function sortContacts() {
    data.sort((a,b) => (a.contactName > b.contactName) ? 1 :((b.contactName > a.contactName) ? -1 :0))
    render()
}
function createContactButton() {
    return /*html*/ `
    <button  id="createButton" onclick="loadCreateContact()">Create Contact</button>
    `
}
function searchContactButton() {
    return /*html*/ `
    <button onclick="searchContactForm()">Search Contact</button>
    `
}
function searchContactForm() {
    const modalContent = /*html*/ `
    <section id="modal-bg">
    <article class="searchContact-modal">
        <form onsubmit="event.preventDefault();searchContact(this);">
            <div>
                <label>Contact Name</label>
                <input name="contactName" required />
            </div>
            <button style="width: 100%;">Search Contact</button>
        </form>
        <button style="width: 100%;" onclick="closeModal()">Close</button>
    </article>
    </section>
`
console.log(modalContent)
const modalEl = document.getElementById('modal')
modalEl.innerHTML = modalContent
}

function searchContact(HTMLform) {

}

// document.getElementById("searchButton").onclick=function() {
//     let searchName = document.getElementById("searchBox").value;
//     console.log(searchName)
    // searchContact(searchName)
// }
// getElementById("searchButton")

function loadCreateContact() {
    // console.log("create contact")
    const modalContent = /*html*/ `
        <section id="modal-bg">
        <article class="contact-card">
            <form onsubmit="event.preventDefault();addContact(this);">
                <div>
                    <label>Name</label>
                    <input name="contactName" required />
                </div>
                <div>
                    <label>Image URL</label>
                    <input name="imageURL" type="url" />
                </div>
                <div>
                    <label>Mobile Number</label>
                    <input name="mobileNumber" required />
                </div>
                <div>
                    <label>Email</label>
                    <input name="email" required />
                </div>
                <div>
                    <label>Address</label>
                    <input name="address" required />
                </div>
                <div> </div>
                <button style="width: 100%;">Add Contact</button>
            </form>
            <button style="width: 100%;" onclick="closeModal()">Close</button>
        </article>
        </section>
    `
    console.log(modalContent)
    const modalEl = document.getElementById('modal')
    modalEl.innerHTML = modalContent
}

function addContact(HTMLform) {
    const form = new FormData(HTMLform)
    const contactName = form.get('contactName')
    const mobileNumber = form.get('mobileNumber')
    const imageURL = form.get('imageURL')
    const email = form.get('email')
    const address = form.get('address')
    fetch('/contacts', {
        method: 'POST',
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({contactName, mobileNumber, imageURL, email, address})
    })
    .then(res => res.json())
    .then(contacts => {
        data.push(contacts)
        render()
    })
    .catch(console.error)
}

function deleteContact() {
    const contacts_id = state.selectedContact.id
    // console.log(contacts_id)
    fetch(`/contacts/${contacts_id}`, {
        method: 'DELETE',
        headers: {
            "Content-Type": "application/json"
        },
    }).then(function(){
        const index = data.findIndex(contact => contact.id === state.selectedContact.id)
        data.splice(index,1)
        closeModal()
    }) 
}
function loadUpdateContact() {
    console.log("update contact")
    const modalContent = /*html*/ `
        <section id="modal-bg">
        <article class="contact-card">
            <form onsubmit="event.preventDefault();updateContact(this);">
                <div>
                    <label>Contact Name</label>
                    <input name="contactName" required />
                </div>
                <div>
                    <label>Image URL for Contact</label>
                    <input name="imageURL" type="url" />
                </div>
                <div>
                    <label>Contact Mobile Number</label>
                    <input name="mobileNumber" required />
                </div>
                <div>
                    <label>Contact Email</label>
                    <input name="email" required />
                </div>
                <div>
                    <label>Contact Address</label>
                    <input name="address" required />
                </div>
                <button style="width: 13rem;">Update Contact</button>
            </form>
            <button onclick="closeModal()">Close</button>
        </article>
        </section>
    `
    console.log(modalContent)
    const modalEl = document.getElementById('modal')
    modalEl.innerHTML = modalContent
}
function updateContact(HTMLform) {
    const form = new FormData(HTMLform)
    const contactName = form.get('contactName')
    const mobileNumber = form.get('mobileNumber')
    const imageURL = form.get('imageURL')
    const email = form.get('email')
    const address = form.get('address')
    const contacts_id = state.selectedContact.id
    fetch(`/contacts/${contacts_id}`, {
        method: 'PUT',
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({contactName, mobileNumber, imageURL, email, address})
    })
    .then(res => res.json())
    .then(contacts => {
        data.push(contacts)
    })
    .then(function(){
        const index = data.findIndex(contact => contact.id === state.selectedContact.id)
        data.splice(index,1)
        closeModal()
    }) 
    .catch(console.error)
}


