package co.uk.barclays.GroupProject;

import org.springframework.data.jpa.repository.JpaRepository;

interface ContactsRepository extends JpaRepository<Contacts, Integer> {}
