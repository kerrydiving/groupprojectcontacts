package co.uk.barclays.GroupProject;

import java.util.List;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
public class ContactsController {
    private ContactsRepository repository;

    public ContactsController(ContactsRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/contacts") // Read All
    public List<Contacts> getContacts() {
        return this.repository.findAll();
    }

    @GetMapping("/contacts/{id}")
    public Contacts getOnecontact(@PathVariable Integer id) {
        return repository.findById(id).get();
    }

    // @GetMapping("/conatcts/{conatctName}")
    // public Contacts getOnecontact(@PathVariable String contactName) {
    //     return repository.findb
    // }

    @PostMapping("/contacts")
    public Contacts createContact(@RequestBody Contacts newcontact) {
        return repository.save(newcontact);
    }

    @PutMapping("/contacts/{id}")
    public Contacts updateOneContact(@PathVariable Integer id, @RequestBody Contacts contactUpdate) {
        return repository.findById(id)
            .map(contact -> {
                contact.setContactName(contactUpdate.getContactName());
                contact.setImageURL(contactUpdate.getImageURL());
                contact.setMobileNumber(contactUpdate.getMobileNumber());
                contact.setAddress(contactUpdate.getAddress());
                contact.setEmail(contactUpdate.getEmail());
                return repository.save(contact);
            }).orElseThrow();
    }

    @DeleteMapping("/contacts/{id}")
    public void deleteOneContact(@PathVariable Integer id) {
        repository.deleteById(id);
    }

}
